/*
 * The MIT License
 *
 * Copyright 2024 Victor Munoz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.horux.tools.versioncontrol;

import com.horux.tools.code.CommandProcessor;
import com.horux.tools.code.CodeGenerator;
import com.horux.tools.versioncontrol.data.GitInfo;
import com.horux.tools.versioncontrol.data.ProjectInfo;
import com.horux.tools.versioncontrol.data.XmlEngine;
import freemarker.template.TemplateException;
import java.io.StringReader;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author victormp
 */
public class GitToSource {

    static String prjectInfoFilename = "project-info.xml";

    /**
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        File projectInfoFile = new File(prjectInfoFilename);
        GitInfo gitInfo;
        try {
            gitInfo = CommandProcessor.getGitInfo();
        } catch (NotGitRepositioryException ex) {
            System.out.println("This is not a git repository");
            return;
        }

        ProjectInfo projectInfo = new ProjectInfo();
        XmlEngine<ProjectInfo> xmlEngine = new XmlEngine(projectInfo);

        if (projectInfoFile.exists()) {
            try {
                projectInfo = xmlEngine.unmarshal(projectInfoFile);
            } catch (JAXBException ex) {
                Logger.getLogger(GitToSource.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            if (!projectInfoFile.createNewFile()) {
                System.out.println("Could not create info file");
                return;
            }
        }

        projectInfo.setGitInfo(gitInfo);
        projectInfo.getBuildInfo().incBuildNumber();
        projectInfo.getBuildInfo().setBuildDateNow();
        
        
        
        var codeGen = new CodeGenerator();  
        
        codeGen.generateFiles(projectInfo);
        

        if (projectInfoFile.canWrite()) {
            try {
                xmlEngine.marshall(projectInfoFile);
            } catch (JAXBException ex) {
                Logger.getLogger(GitToSource.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.println("Cannot write into file");
        }

    }

}
