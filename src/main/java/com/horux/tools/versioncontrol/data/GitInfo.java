/*
 * The MIT License
 *
 * Copyright 2024 Victor Munoz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.horux.tools.versioncontrol.data;

import java.util.Date;


import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author victormp
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class GitInfo {

    /**
     *
     */
    @XmlElement(name = "aheadOfCommit", required = true)
    protected boolean aheadOfCommit;
    
    /**
     *
     */
    @XmlElement(name = "aheadOfCommitExcludeUntracked", required = true)
    protected boolean aheadOfCommitExcludeUntracked;
    
    /**
     *
     */
    @XmlElementWrapper(name = "tags", required = false)
    @XmlElement(name = "tag", required = false)
    protected List<String> tags;

    /**
     *
     */
    @XmlElement(name = "commitComment", required = true)
    protected String commitComment;

    /**
     *
     */
    @XmlElement(name = "commitDate", required = true, defaultValue = "")
    protected Date commitDate;

    /**
     *
     */
    @XmlElement(name = "hash", required = true)
    protected String hash;

    /**
     *
     */
    @XmlElement(name = "shortHash", required = true)
    protected String shortHash;

    /**
     *
     */
    public GitInfo() {
        tags = new ArrayList<> ();
    }
    
    
    
    /**
     * Get the value of AheadOfCommit
     *
     * @return the value of AheadOfCommit
     */
    public boolean isAheadOfCommit() {
        return aheadOfCommit;
    }

    /**
     * Set the value of AheadOfCommit
     *
     * @param AheadOfCommit new value of AheadOfCommit
     */
    public void setAheadOfCommit(boolean AheadOfCommit) {
        this.aheadOfCommit = AheadOfCommit;
    }

    

    /**
     * Get the value of AheadOfCommitExcludeUntracked
     *
     * @return the value of AheadOfCommitExcludeUntracked
     */
    public boolean isAheadOfCommitExcludeUntracked() {
        return aheadOfCommitExcludeUntracked;
    }

    /**
     * Set the value of AheadOfCommitExcludeUntracked
     *
     * @param AheadOfCommitExcludeUntracked new value of
     * AheadOfCommitExcludeUntracked
     */
    public void setAheadOfCommitExcludeUntracked(boolean AheadOfCommitExcludeUntracked) {
        this.aheadOfCommitExcludeUntracked = AheadOfCommitExcludeUntracked;
    }

    /**
     * Get the value of tags
     *
     * @return the value of tags
     */
    public List<String> getTags() {
        return tags;
    }

    /**
     * Set the value of tags
     *
     * @param tags new value of tags
     */
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    //
    /**
     * Get the value of hash
     *
     * @return the value of hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * Set the value of hash
     *
     * @param hash new value of hash
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * Get the value of shortHash
     *
     * @return the value of shortHash
     */
    public String getShortHash() {
        return shortHash;
    }

    /**
     * Set the value of shortHash
     *
     * @param shortHash new value of shortHash
     */
    public void setShortHash(String shortHash) {
        this.shortHash = shortHash;
    }

    /**
     * Get the value of commitComment
     *
     * @return the value of commitComment
     */
    public String getCommitComment() {
        return commitComment;
    }

    /**
     * Set the value of commitComment
     *
     * @param commitComment new value of commitComment
     */
    public void setCommitComment(String commitComment) {
        this.commitComment = commitComment;
    }

    /**
     * Get the value of commitDate
     *
     * @return the value of commitDate
     */
    public Date getCommitDate() {
        return commitDate;
    }

    /**
     *
     * @return
     */
    public String getFormatedCommitInstant() {
        return commitDate.toInstant().toString();
    }
    
    /**
     * Set the value of commitDate
     *
     * @param commitDate new value of commitDate
     */
    public void setCommitDate(Date commitDate) {
        this.commitDate = commitDate;
    }

    private String tagDescription;

    /**
     * Get the value of tagDescription
     *
     * @return the value of tagDescription
     */
    public String getTagDescription() {
        return tagDescription;
    }

    /**
     * Set the value of tagDescription
     *
     * @param tagDescription new value of tagDescription
     */
    public void setTagDescription(String tagDescription) {
        this.tagDescription = tagDescription;
    }

}
