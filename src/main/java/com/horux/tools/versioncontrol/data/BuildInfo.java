/*
 * The MIT License
 *
 * Copyright 2024 Victor Munoz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.horux.tools.versioncontrol.data;

import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import java.time.Instant;
import java.util.Date;

/**
 *
 * @author victormp
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class BuildInfo {

    /**
     *
     */
    @XmlElement(name = "buildDate", required = true)
    protected Date buildDate;

    /**
     *
     */
    @XmlElement(name = "buildNumber", required = true)
    protected Integer buildNumber;

    /**
     *
     */
    public BuildInfo() {
        buildNumber = 0;
    }

    /**
     * Get the value of buildNumber
     *
     * @return the value of buildNumber
     */
    public int getBuildNumber() {
        return buildNumber;
    }

    /**
     * Set the value of buildNumber
     *
     * @param buildNumber new value of buildNumber
     */
    public void setBuildNumber(int buildNumber) {
        this.buildNumber = buildNumber;
    }

    /**
     *
     */
    public void incBuildNumber() {
        this.buildNumber++;
    }

    /**
     * Get the value of buildDate
     *
     * @return the value of buildDate
     */
    public Date getBuildDate() {
        return buildDate;
    }

    /**
     * Set the value of buildDate
     *
     * @param buildDate new value of buildDate
     */
    public void setBuildDate(Date buildDate) {
        this.buildDate = buildDate;
    }

    /**
     *
     * @return
     */
    public String getFormatedBuildInstant() {
        return buildDate.toInstant().toString();
    }
    
    /**
     *
     */
    public void setBuildDateNow(){
        this.buildDate = Date.from(Instant.now());
    }

}
