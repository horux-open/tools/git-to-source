/*
 * The MIT License
 *
 * Copyright 2024 Victor Munoz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.horux.tools.versioncontrol.data;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

/**
 *
 * @author victormp
 * @param <T>
 */
public class XmlEngine<T>{

    
    
    T pojo = null;
    Class<?> tClass;
    
    /**
     *
     * @param object
     */
    public XmlEngine(T object){
        pojo = object;
        tClass = object.getClass();
    }

    /**
     *
     * @param xml
     * @return 
     * @throws jakarta.xml.bind.JAXBException 
     */
    public T unmarshal(String xml) throws JAXBException {  
        JAXBContext context = JAXBContext.newInstance(tClass);
        Unmarshaller um = context.createUnmarshaller();
        pojo = (T) um.unmarshal(new StringReader(xml));
        return pojo;  
    }
    
    /**
     *
     * @param file
     * @return
     * @throws JAXBException
     */
    public T unmarshal(File file) throws JAXBException {  
        JAXBContext context = JAXBContext.newInstance(tClass);
        Unmarshaller um = context.createUnmarshaller();
        pojo = (T) um.unmarshal(file);
        return pojo;  
    }
    
    /**
     *
     * @return
     * @throws JAXBException
     */
    public String marshall() throws JAXBException{
        JAXBContext context = JAXBContext.newInstance(tClass);
        Marshaller mrsh = context.createMarshaller();
        StringWriter strWriter = new StringWriter();
        mrsh.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        mrsh.marshal(pojo, strWriter);
        return strWriter.toString();
    }
    
    /**
     *
     * @param file
     * @throws IOException
     * @throws JAXBException
     */
    public void marshall(File file) throws IOException, JAXBException{
        JAXBContext context = JAXBContext.newInstance(tClass);
        Marshaller mrsh = context.createMarshaller();
        mrsh.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        mrsh.marshal(pojo, file);
    }

    /**
     *
     * @return
     */
    public T getObject() {
        return pojo;
    }
    
    
    
    

}
