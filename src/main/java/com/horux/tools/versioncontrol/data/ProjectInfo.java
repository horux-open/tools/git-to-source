/*
 * The MIT License
 *
 * Copyright 2024 Victor Munoz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.horux.tools.versioncontrol.data;

import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author victormp
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "com.horux.tools.versioncontrol.data.ProjectInfo")
public class ProjectInfo {
    
    /**
     *
     */
    @XmlElement(name = "gitInfo", required = true)
    protected GitInfo gitInfo;
    
    /**
     *
     */
    @XmlElement(name = "buildInfo", required = true)
    protected BuildInfo buildInfo;

    /**
     *
     */
    public ProjectInfo() {
        gitInfo = new GitInfo();
        buildInfo = new BuildInfo();
        
    }
    
    /**
     * Get the value of gitInfo
     *
     * @return the value of gitInfo
     */
    public GitInfo getGitInfo() {
        return gitInfo;
    }

    /**
     * Set the value of gitInfo
     *
     * @param gitInfo new value of gitInfo
     */
    public void setGitInfo(GitInfo gitInfo) {
        this.gitInfo = gitInfo;
    }

       

    /**
     * Get the value of buildInfo
     *
     * @return the value of buildInfo
     */
    public BuildInfo getBuildInfo() {
        return buildInfo;
    }

    /**
     * Set the value of buildInfo
     *
     * @param buildInfo new value of buildInfo
     */
    public void setBuildInfo(BuildInfo buildInfo) {
        this.buildInfo = buildInfo;
    }   
}
