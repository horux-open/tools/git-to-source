/*
 * The MIT License
 *
 * Copyright 2024 Victor Munoz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.horux.tools.code;


import com.horux.tools.code.CommandProcessor;
import com.horux.tools.versioncontrol.NotGitRepositioryException;
import com.horux.tools.versioncontrol.data.GitInfo;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author victormp
 */
public class CommandProcessorWindows {
    
    static String CommandString = "cmd.exe";
    static String CommandCSwitch = "/c";
    
    
    
    static String GetFromCommand(String gitCommand) throws IOException, NotGitRepositioryException{
                ProcessBuilder processBuilder = new ProcessBuilder();
        // Windows
        processBuilder.command(CommandString, CommandCSwitch, gitCommand);

        Process process = processBuilder.start();

        BufferedReader reader
                = new BufferedReader(new InputStreamReader(process.getInputStream()));

        String line = reader.readLine();

        if (line == null) {
            //throw new NotGitRepositioryException("Couldn't read any information. Maybe this is not a git repository");
            return "";
        }

        if (line.toLowerCase().contains("fatal")) {
            throw new NotGitRepositioryException("FATAL: Maybe this is not a git repository");
        }

        return line;
    }
    
    
        static ArrayList<String> GetListFromCommand(String gitCommand) throws IOException, NotGitRepositioryException{
        
        ArrayList<String> tagList = new ArrayList<>();
            
        ProcessBuilder processBuilder = new ProcessBuilder();
        // Windows
        processBuilder.command(CommandString, CommandCSwitch, gitCommand);

        Process process = processBuilder.start();

        BufferedReader reader
                = new BufferedReader(new InputStreamReader(process.getInputStream()));

        String line;
        while ( (line = reader.readLine()) != null){
            if (line.toLowerCase().contains("fatal")) {
                throw new NotGitRepositioryException("FATAL: Maybe this is not a git repository");
            }
            
            tagList.add(line);
            
        }
        
        

        return tagList;
    }
    
    
          
    
    
        

    static GitInfo getGitInfo() throws NotGitRepositioryException, IOException {
        GitInfo pGitInfo = new GitInfo();
        pGitInfo.setHash(GetFromCommand(CommandProcessor.GitExtractHash));
        pGitInfo.setShortHash(GetFromCommand(CommandProcessor.GitExtractShortHash));
        pGitInfo.setTags(GetListFromCommand(CommandProcessor.GitExtractTags));
        pGitInfo.setTagDescription(GetFromCommand(CommandProcessor.GitDescribeTag));
        //pGitInfo.setTag(GetFromCommand(GitExtractTagPattern));
        
        String status = GetFromCommand(CommandProcessor.GitCurrentStatus);
        pGitInfo.setAheadOfCommit(!status.isEmpty());
        
        
        String statusExcludingUntracked = GetFromCommand(CommandProcessor.GitCurrentStatusExcludingUntracked);
        pGitInfo.setAheadOfCommitExcludeUntracked(!statusExcludingUntracked.isEmpty());
        
        
        String commiterDate = GetFromCommand(CommandProcessor.GitCommiterDate);
        pGitInfo.setCommitDate(Date.from(Instant.parse(commiterDate)));
        
        return pGitInfo;
    }

    static String getHostname() throws IOException, NotGitRepositioryException {
        return GetFromCommand(CommandProcessor.hostname);
    }

   

}
