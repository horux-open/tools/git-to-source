/*
 * The MIT License
 *
 * Copyright 2024 Victor Munoz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.horux.tools.code;

import com.horux.tools.versioncontrol.data.ProjectInfo;
import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Victor Munoz
 * @since 1.0 Helper class that creates a configuration instance of FreeMaker
 */
public class CodeGenerator {

    Configuration configuration;

    private final static Map<String, String> templateOutputMap;

    public static Map<String, String> getTemplateOutputMap() {
        return templateOutputMap;
    }

    static {
        templateOutputMap = new HashMap<>();
        templateOutputMap.put("project-info-extern.fh", "project-info-extern.h");
        templateOutputMap.put("project-info-extern.fc", "project-info-extern.c");
        templateOutputMap.put("project-info-defines.fh", "project-info-defines.h");
    }
    
    static String freemakerVersion = "2.3.32";
    static String templateDirectory = "/views";
    static String encoding = "UTF-8";
    
    /**
     *
     * @throws MalformedTemplateNameException
     * @throws ParseException
     * @throws IOException
     */
    public CodeGenerator() throws MalformedTemplateNameException, ParseException, IOException {
        configuration = new Configuration(new Version(freemakerVersion));
        configuration.setClassForTemplateLoading(CodeGenerator.class, templateDirectory);
        configuration.setDefaultEncoding(encoding);
    }

  

    public void templateToWriter(ProjectInfo projectInfo, String sTemplate, Writer wrt) throws TemplateException, IOException {
        var template = configuration.getTemplate(sTemplate);
        template.process(projectInfo, wrt);
        wrt.flush();
    }

    /**
     *
     * @param projectInfo
     */
    public void generateFiles(ProjectInfo projectInfo) {
        templateOutputMap.forEach((String k, String v) -> {
            try (java.io.FileWriter out = new FileWriter(v)) {
                CodeGenerator.this.templateToWriter(projectInfo, k, out);
            }catch (IOException | TemplateException ex) {
                Logger.getLogger(CodeGenerator.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }
    
  

}
