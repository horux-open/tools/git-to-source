/*
 * The MIT License
 *
 * Copyright 2024 Victor Munoz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.horux.tools.code;



import com.horux.tools.versioncontrol.NotGitRepositioryException;
import com.horux.tools.versioncontrol.data.GitInfo;

import java.io.IOException;
import org.apache.commons.lang3.SystemUtils;

/**
 *
 * @author victormp
 */
public class CommandProcessor {
    
    /**
     *
     */
    public static String GitExtractHash                     = "git rev-parse HEAD";

    /**
     *
     */
    public static String GitExtractShortHash                = "git rev-parse --short HEAD";

    /**
     *
     */
    public static String GitExtractTags                     = "git tag --list --sort=-taggerdate";

    /**
     *
     */
    public static String GitExtractTagPattern               = "git tag --list '%s'";

    /**
     *
     */
    public static String GitCurrentStatus                   = "git status -s -uall";

    /**
     *
     */
    public static String GitCurrentStatusExcludingUntracked = "git status -s -uno";

    /**
     *
     */
    public static String GitDescribeTag                     = "git describe --tags";

    /**
     *
     */
    public static String GitCommiterDate                    = "git show -s --format=%cI";
    
    
    public static String hostname                           = "hostname";
    
    /**
     *
     * @return
     * @throws IOException
     * @throws NotGitRepositioryException
     */
    public static GitInfo getGitInfo() throws IOException, NotGitRepositioryException{

        
        if (SystemUtils.IS_OS_WINDOWS) {
            return CommandProcessorWindows.getGitInfo();
            //return ProcessWindows();
        }
        
        return null;
    }
    
    public String getHostname() throws IOException, NotGitRepositioryException{
        if (SystemUtils.IS_OS_WINDOWS){
            return CommandProcessorWindows.getHostname();
        }
        return null;
    }
}
