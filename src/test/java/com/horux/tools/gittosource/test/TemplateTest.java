/*
 * The MIT License
 *
 * Copyright 2024 Victor Munoz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.horux.tools.gittosource.test;

import com.horux.tools.code.CodeGenerator;
import com.horux.tools.versioncontrol.data.ProjectInfo;
import com.horux.tools.versioncontrol.data.XmlEngine;
import freemarker.core.ParseException;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.TemplateException;
import jakarta.xml.bind.JAXBException;
import java.io.IOException;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.jupiter.api.Test;

/**
 *
 * @author victormp
 */
public class TemplateTest {

    static final String xml = """
                    <?xml version="1.0" encoding="UTF-8"?>\r
                    <com.horux.tools.versioncontrol.data.ProjectInfo>\r
                        <gitInfo>\r
                            <aheadOfCommit>true</aheadOfCommit>
                            <aheadOfCommitExcludeUntracked>true</aheadOfCommitExcludeUntracked>\r
                            <tags>
                                <tag>1.0.beta1</tag>
                                <tag>1.0.beta2</tag>
                                <tag>1.0.beta3</tag>
                                <tag>1.0.beta4</tag>
                                <tag>1.0.beta5</tag>
                                <tag>1.0.beta6</tag>
                                <tag>1.0.alpha1</tag>
                                <tag>1.0.alpha2</tag>
                            </tags>
                            <hash>db07568d6121c8e527948c42cec599bc863dd656</hash>
                                  
                                
                                  
                                  
                            <shortHash>db07568d</shortHash>
                            <commitComment>Just a comment</commitComment>
                            <commitDate>2022-08-01T12:33:50.709+03:00</commitDate>
                            <tagDescription>1.0.beta6-1-g6c50b40</tagDescription>
                        </gitInfo>
                        <buildInfo>
                            <buildNumber>10254</buildNumber>
                            <buildDate>2022-08-01T12:33:51.381Z</buildDate>
                        </buildInfo>
                    </com.horux.tools.versioncontrol.data.ProjectInfo>
                    """;

    @Test
    public void printStringsFromTmplates() throws MalformedTemplateNameException, ParseException, IOException, JAXBException, TemplateException {
        var codeGen = new CodeGenerator();
        ProjectInfo projectInfo = new ProjectInfo();
        XmlEngine<ProjectInfo> xmlEngine = new XmlEngine(projectInfo);
        projectInfo = xmlEngine.unmarshal(xml);

        final ProjectInfo lmdProjectInfo = projectInfo;

        CodeGenerator.getTemplateOutputMap().forEach((String k, String v) -> {
            try (StringWriter out = new StringWriter()) {
                codeGen.templateToWriter(lmdProjectInfo, k, out);

                System.out.println(out.getBuffer().toString());
            } catch (IOException | TemplateException ex) {
                Logger.getLogger(TemplateTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

    }

    @Test
    public void createFilesFromTmplates() throws MalformedTemplateNameException, ParseException, IOException, JAXBException, TemplateException {
        var codeGen = new CodeGenerator();
        ProjectInfo projectInfo = new ProjectInfo();
        XmlEngine<ProjectInfo> xmlEngine = new XmlEngine(projectInfo);
        projectInfo = xmlEngine.unmarshal(xml);
        codeGen.generateFiles(projectInfo);
    }

}
