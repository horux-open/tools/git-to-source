/*
 * The MIT License
 *
 * Copyright 2024 Victor Munoz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.horux.tools.gittosource.test;


import com.horux.tools.versioncontrol.data.ProjectInfo;
import com.horux.tools.versioncontrol.data.XmlEngine;
import jakarta.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.Date;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

/**
 *
 * @author victormp
 */
public class XmllTest {

    static final String xml = """
                    <?xml version="1.0" encoding="UTF-8"?>\r
                    <com.horux.tools.versioncontrol.data.ProjectInfo>\r
                        <gitInfo>\r
                            <aheadOfCommit>true</aheadOfCommit>
                            <aheadOfCommitExcludeUntracked>true</aheadOfCommitExcludeUntracked>\r
                            <tags>
                                <tag>1.0.beta1</tag>
                                <tag>1.0.beta2</tag>
                                <tag>1.0.beta3</tag>
                                <tag>1.0.beta4</tag>
                                <tag>1.0.beta5</tag>
                                <tag>1.0.beta6</tag>
                                <tag>1.0.alpha1</tag>
                                <tag>1.0.alpha2</tag>
                            </tags>
                            <hash>6c00b40f262039f3b585f399b8b12d0de70f3312</hash>
                            <shortHash>6c00b40</shortHash>
                            <commitComment>Just a comment</commitComment>
                            <commitDate>2022-08-01T12:33:50.709+03:00</commitDate>
                            <tagDescription>1.0.beta6-1-g6c00b40</tagDescription>
                        </gitInfo>
                        <buildInfo>
                            <buildNumber>448</buildNumber>
                            <buildDate>2022-08-01T12:33:51.381Z</buildDate>
                        </buildInfo>
                    </com.horux.tools.versioncontrol.data.ProjectInfo>
                    """;

    @Test
    public void testUnmarshall() throws JAXBException {
        ProjectInfo projectInfo = new ProjectInfo();
        XmlEngine<ProjectInfo> xmlEngine = new XmlEngine(projectInfo);
        projectInfo = xmlEngine.unmarshal(xml);
        Assertions.assertEquals(true, projectInfo.getGitInfo().isAheadOfCommit());
        Assertions.assertEquals(true, projectInfo.getGitInfo().isAheadOfCommitExcludeUntracked());
        Assertions.assertEquals(8, projectInfo.getGitInfo().getTags().size());
        Assertions.assertEquals("1.0.beta1", projectInfo.getGitInfo().getTags().get(0));
        Assertions.assertEquals("1.0.beta2", projectInfo.getGitInfo().getTags().get(1));
        Assertions.assertEquals("6c00b40f262039f3b585f399b8b12d0de70f3312", projectInfo.getGitInfo().getHash());
        Assertions.assertEquals("6c00b40", projectInfo.getGitInfo().getShortHash());
        Assertions.assertEquals("Just a comment", projectInfo.getGitInfo().getCommitComment());
        Date date = Date.from(Instant.parse("2022-08-01T12:33:50.709+03:00"));
        Assertions.assertEquals(date, projectInfo.getGitInfo().getCommitDate());
        Assertions.assertEquals("1.0.beta6-1-g6c00b40", projectInfo.getGitInfo().getTagDescription());
        Assertions.assertEquals(448, projectInfo.getBuildInfo().getBuildNumber());
        date = Date.from(Instant.parse("2022-08-01T12:33:51.381Z"));
        Assertions.assertEquals(date, projectInfo.getBuildInfo().getBuildDate());
    }

    @Test
    public void createXmlNewFile() throws IOException, JAXBException {
        ProjectInfo projectInfo = new ProjectInfo();
        projectInfo.getGitInfo().setAheadOfCommit(true);
        projectInfo.getGitInfo().setAheadOfCommitExcludeUntracked(true);
        projectInfo.getGitInfo().getTags().add("1.0.beta1");
        projectInfo.getGitInfo().getTags().add("1.0.beta2");
        projectInfo.getGitInfo().getTags().add("1.0.beta3");
        projectInfo.getGitInfo().getTags().add("1.0.beta4");
        projectInfo.getGitInfo().setHash("6c00b40f262039f3b585f399b8b12d0de70f3312");
        projectInfo.getGitInfo().setShortHash("6c00b40");
        projectInfo.getGitInfo().setCommitComment("Test Info commit");
        projectInfo.getGitInfo().setCommitDate(Date.from(Instant.now()));
        projectInfo.getGitInfo().setTagDescription("1.0.beta4-1-g6c00b40");
        projectInfo.getBuildInfo().setBuildDate(Date.from(Instant.now()));
        projectInfo.getBuildInfo().setBuildNumber(545);
        XmlEngine<ProjectInfo> xmlEngine = new XmlEngine(projectInfo);

        File file = new File("testFile.xml");

        if (!file.exists()) {
            if (!file.createNewFile()) {
                throw new IOException("Could not create file");
            }
        }

        if (!file.canWrite()) {
            throw new IOException("Cannot write to file");
        }
        xmlEngine.marshall(file);
        
        ProjectInfo projectInfoLoopback = new ProjectInfo();
        xmlEngine = new XmlEngine(projectInfoLoopback);
        projectInfoLoopback = xmlEngine.unmarshal(file);
        
        Assertions.assertEquals(projectInfo.getBuildInfo().getBuildNumber(), projectInfoLoopback.getBuildInfo().getBuildNumber());

    }
}
